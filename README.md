The beam width parameter of a laser beam with a Gaussian profile can be
measured with the _knife-edge method_.

This package includes a general class for plotting and fitting of beam profiles,
along with more specific classes for different methods of obtaining the
profiles: swinging ruler, manual micrometer positioning, my custom LEGO NXT
profiler.
